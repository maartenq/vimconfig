" after/ftplugin/mail.vim

setlocal textwidth=79
setlocal colorcolumn=80
setlocal spell spelllang=nl,en_gb
setlocal complete+=k
