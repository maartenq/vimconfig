" after/ftplugin/markdown.vim

setlocal nofoldenable
setlocal textwidth=79
setlocal colorcolumn=80
setlocal spell spelllang=nl,en_gb
setlocal complete+=k
" Align GitHub-flavored Markdown tables
vnoremap <Leader><Bslash> :EasyAlign*<Bar><Enter>
