" ~/.vim/.vimrc
" vim: si et ts=4 sw=4 sts=4 fileencoding=UTF-8 ft=vim

" Plugins
call plug#begin('~/.vim/plugged')
Plug 'FooSoft/vim-argwrap'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'airblade/vim-rooter'
Plug 'dhruvasagar/vim-table-mode'
Plug 'itchyny/lightline.vim'         " A light and configurable statusline/tabline plugin.
Plug 'junegunn/vim-easy-align'
Plug 'psf/black'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vim-syntastic/syntastic'       " Syntax checking hacks for vim
Plug 'will133/vim-dirdiff'
call plug#end()


" Encoding
set encoding=utf-8
" set fileencoding=utf-8
scriptencoding utf-8

" buffers
set hidden

" Basics
set nocompatible
set visualbell
set nobackup
set modeline

"Additions
set title
set wildmenu
set ruler
set number
set relativenumber
set laststatus=2

" Default Indents
" Alternative configs for file types live here:
" ~/.config/nvim/ftplugin
set autoindent    " Turn on auto indent
set smartindent   " Do it smartly
set cindent       " More stricter rules for C programs
set expandtab     " Turn tabs into whitespace
set tabstop=2     " Set tab character to 4 characters
set shiftwidth=2  " Indent width for autoindent
set softtabstop=2 " Makes the spaces feel like real tabs

" Show tabs and trailing space
"set list listchars=tab:▸\ ,trail:.
set list listchars=tab:→\ ,nbsp:␣,trail:·

" Search
set hlsearch
set incsearch  " Turn on incremental search
set ignorecase " Ignore case
set smartcase  " (except explicit caps)

" Color Scheme
syntax on
set background=dark
set t_Co=256
colorscheme sahara
highlight LineNr ctermfg=grey

" Easier split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" More natural split opening
set splitbelow
set splitright

" Map leader to <space>
nnoremap <SPACE> <Nop>
let mapleader=' '

" netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3

" Run isort
nnoremap <leader>is :!isort %<CR>

" Moving around windows
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" Toggle QuickFix list
function! ToggleQuickFix()
    if empty(filter(getwininfo(), 'v:val.quickfix'))
        copen
    else
        cclose
    endif
endfunction

nnoremap <leader>q :call ToggleQuickFix()<CR>

" Explore in split
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>

" Remember cursor position
augroup REMEMBER_CURSOR_POSITION
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

" Paste into a virtual block without filling the default register
vnoremap <leader>p "_dP

" Paste the OS paste buffer
nnoremap <leader>p "*p<CR>

" Plugin  dhruvasagar/vim-table-mode
let g:table_mode_corner='|'


" Plug 'FooSoft/vim-argwrap'
let g:argwrap_tail_comma = 1
nnoremap <leader>a :ArgWrap<CR>

" Plug 'psf/black'
let g:black_linelength = 79
nnoremap <leader>bl :Black<CR>

" Plugin tpope/vim-fugitive
nnoremap <leader>gs :G<CR>
nnoremap <leader>gdl :diffget //2<CR>
nnoremap <leader>gdr :diffget //3<CR>

" Plugin will133/vim-dirdiff
let g:DirDiffExcludes = '*.terraform,*.terraform.lock.hcl'

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
