" after/ftplugin/mail.vim
setlocal ts=2  sw=2 sts=2
setlocal textwidth=79
setlocal colorcolumn=80
setlocal spell spelllang=nl,en_gb
setlocal complete+=k
